package com.example.cse438.studio2.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.example.cse438.studio2.model.Product

class ProductViewModel(application: Application): AndroidViewModel(application) {
    private var _productsList: MutableLiveData<ArrayList<Product>> = MutableLiveData()

    fun getNewProducts(): MutableLiveData<ArrayList<Product>> {
        //TODO: Implement this method based on LoopBack API's documentation and the below function getProductsByQueryText

        return _productsList
    }

    fun getProductsByQueryText(query: String): MutableLiveData<ArrayList<Product>> {
        loadProducts("?filter={\"where\":{\"name\":{\"like\":\".*$query.*\",\"options\":\"i\"}}}")
        return _productsList
    }

    private fun loadProducts(query: String) {
        //TODO: Implement this function
    }

    //TODO: Implement the AsyncTask below
}